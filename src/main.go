package main

import (
	"log"
	"os"
	"time"

	uuid "github.com/satori/go.uuid"
	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/mem"
	"gopkg.in/ini.v1"
)

func main() {
	log.Println("Application my-golang-logger started")
	valid := true
	pid := os.Getpid()
	hname, err := os.Hostname()
	if err != nil {
		panic(err)
	}
	count := 0
	cfg, err := ini.Load("logger.ini")
	if err != nil {
		panic(err)
	}
	sleep, _ := cfg.Section("logger").Key("sleep").Int()
	secondWait := time.Duration(sleep)
	// This loop continues while "valid" is true.
	for valid {
		count++
		time.Sleep(secondWait * time.Second)
		myuuid := uuid.NewV4().String()
		// cpu - get CPU utilization percentage of cores
		percentage, err := cpu.Percent(0, true)
		if err != nil {
			panic(err)
		}
		// memoryUsedPercent
		vmStat, err := mem.VirtualMemory()
		if err != nil {
			panic(err)
		}
		log.Println(hname, count, myuuid, pid, percentage, vmStat.UsedPercent)
	}
}
