#!/bin/bash
export BUILDKIT_PROGRESS=plain
export DOCKER_BUILDKIT=1

CNAME="golang-logger"
ver=$(cat version)      &&      echo "Version:$ver"
docker build -t ddanilov77/$CNAME:$ver .
docker rmi $(docker images | grep "none" | awk '{print $3}')
docker images | grep "ddanilov77/$CNAME"

DNAME="$CNAME-$ver"     &&      echo "DNAME="$DNAME
docker run --name $DNAME --rm -d ddanilov77/$CNAME:$ver
sleep 2
echo $(docker ps -a | grep $DNAME | awk '{print $1}')
docker logs -f $(docker ps -a | grep "$DNAME" | awk '{print $1}')
