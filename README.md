# my-golang-logger
<br>
My golang logger for testing
<hr>
/src/main.go<br>
/src/logger.ini<br>
<hr>
docker build -t ddanilov77/my-golang-logger .<br>
<hr>
2021/05/19 14:36:26 Application my-go-logger started at time UTC:2021-05-19 14:36:26 Wednesday<br>
2021/05/19 14:36:33 Worker20 1 b529c80f-2498-4caa-b74e-7427d971f6cd 22642 [1.2893982808012003 3.295128939823572 1.4347202295536923 2.275960170698554] 18.798293957577997<br>
<hr>
App output logs every N second<br>
log output format:<br>
datetime, hostname, counter, uuid, pid, [cpu_utilization of cores], memory.UsedPercent<br>
<br>
datetime: standart YYYY/MM/DD hh:mm:ss<br>
hostname: name of host{pod}<br>
counter: simple counter<br>
uuid: universally unique identifier<br>
pid: Process IDentifier<br>
[cpu_utilization of cores]: CPU utilization for each Core<br>
memory.UsedPercent: Memory Used Percent<br>
