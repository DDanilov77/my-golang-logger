FROM golang:1.15-alpine AS build
WORKDIR /go/src/app
COPY src/ .
ENV CGO_ENABLED=0
RUN go get && go build -o logger main.go

FROM alpine:3.12
LABEL Maintainer="Denis Danilov <ddanilov77@mail.ru>" \
      Description="Lightweight container with output sample logs based on Alpine Linux."
WORKDIR /app
COPY --from=build /go/src/app/logger /app/
COPY --from=build /go/src/app/logger.ini /app/
CMD ["/app/logger"]
